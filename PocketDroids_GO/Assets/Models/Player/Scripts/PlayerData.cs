﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerData
{
    private int xp;
    private int requiredXp;
    private int lvlBase;
    private int lvl;

    public int Xp { get => xp; }
    public int RequiredXp { get => requiredXp; }
    public int LvlBase { get => lvlBase; }
    public int Lvl { get => lvl; }

    // Constructor
    public PlayerData(Player player)
    {
        xp = player.Xp;
        requiredXp = player.RequiredXp;
        lvlBase = player.LvlBase;
        lvl = player.Lvl;
    }
}
