﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private int xp = 0;
    [SerializeField] private int requiredXP = 100;
    [SerializeField] private int lvlBase = 100;

    private int lvl = 1;
    private string path;

    public int Xp
    {
        get { return xp; }
    }

    public int RequiredXp
    {
        get { return requiredXP; }
    }

    public int LvlBase
    {
        get { return lvlBase; }
    }

    public int Lvl
    {
        get { return lvl; }
    }

    // Start is called before the first frame update
    void Start()
    {
        path = Application.persistentDataPath + "/player.dat"; // Percorso in cui salvero' i dati
        Load(); // If we don't have a data file we InitLevelData, else we load our existing player data
    }

    public void AddXp(int exp)
    {
        this.xp += Mathf.Max(0, exp);
        InitLevelData(); // Handling the level design
        Save();
    }

    public void InitLevelData()
    {
        lvl = (xp / lvlBase) + 1;
        requiredXP = lvlBase * lvl;
    }

    // Common way used to save big data files in Unity
    private void Save()
    {
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream file = File.Create(path);
        PlayerData data = new PlayerData(this);
        binaryFormatter.Serialize(file, data);
        file.Close();
    }

    // Common way used to load big data files in Unity
    private void Load()
    {
        if (File.Exists(path))
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream file = File.Open(path, FileMode.Open);
            PlayerData data = (PlayerData)binaryFormatter.Deserialize(file);
            file.Close();
            LoadPlayerData(data);

            /// TODO Setup the droids to load up
        }
        else
        {
            InitLevelData();
        }
    }

    private void LoadPlayerData(PlayerData data)
    {
        xp = data.Xp;
        requiredXP = data.RequiredXp;
        lvlBase = data.LvlBase;
        lvl = data.Lvl;
    }

    public void ResetData()
    {
        File.Delete(path);
        Debug.Log("Deleted Saveing file");
        Load();
    }
}
