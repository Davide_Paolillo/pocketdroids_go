﻿using Mapbox.Unity.Location;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    private DeviceLocationProvider deviceLocationProvider;
    private Location currentLocation;
    private GameManager gameManager;

    private bool isInRange;
    private int speedInMetersPerSeconds;
    private int movSpeed;
    private bool isTooFast;
    private bool pauseGame;
    private float secToPause;
    private bool isRunning;
    private float secondToWaitBeforeRestartingFastCounter;
    private int maxMovSpeed;

    public int MovSpeed { get => movSpeed; }
    public int MaxMovSpeed { get => maxMovSpeed; }
    public bool IsInRange { get => isInRange; }

    private void Awake()
    {
        deviceLocationProvider = FindObjectOfType<DeviceLocationProvider>();
        speedInMetersPerSeconds = 0;
        StartCoroutine(CalculateSpeedInMetersPerSecond());
    }

    void Start()
    {
        isInRange = false;
        gameManager = FindObjectOfType<GameManager>();
        isTooFast = false;
        pauseGame = false;
        secToPause = 300.0f;
        isRunning = false;
        secondToWaitBeforeRestartingFastCounter = 90.0f;
        maxMovSpeed = 7;
        StartCoroutine(CheckSpeed());
    }

    private IEnumerator CheckSpeed()
    {
        while (true)
        {
            movSpeed = speedInMetersPerSeconds;
            //GameObject.FindGameObjectWithTag("DebugText").GetComponent<Text>().text = movSpeed.ToString();
            if (movSpeed > maxMovSpeed)
            {
                isTooFast = true;
            }
            else
            {
                isTooFast = false;
            }
            yield return new WaitForSeconds(10.0f);
            movSpeed = speedInMetersPerSeconds;
            //GameObject.FindGameObjectWithTag("DebugText").GetComponent<Text>().text = movSpeed.ToString();

            if (movSpeed > maxMovSpeed && isTooFast)
            {
                pauseGame = true;
            }
            else if(movSpeed > maxMovSpeed)
            {
                isTooFast = true;
            }
            else
            {
                isTooFast = false;
            }

            if (pauseGame)
            {
                // Pause the game for 5 minutes
                gameManager.StartPause(secToPause);
                pauseGame = false;
            }
            yield return new WaitForSeconds(10.0f);
        }
    }

    private IEnumerator CalculateSpeedInMetersPerSecond()
    {
        Location prevLocation = new Location();
        while (true)
        {
            prevLocation = deviceLocationProvider.CurrentLocation;
            yield return new WaitForSeconds(5.0f);
            int distanceCoveredInOneSecond = CalculateDistanceInMeters(deviceLocationProvider.CurrentLocation, prevLocation) / 5;
            speedInMetersPerSeconds = distanceCoveredInOneSecond > 50 ? 0 : distanceCoveredInOneSecond;
            if (speedInMetersPerSeconds != 0)
            {
                Debug.Log("Speed in m/s : " + speedInMetersPerSeconds);
            }
        }
    }

    private int CalculateDistanceInMeters(Location first, Location second)
    {
        if (!first.Equals(second))
        {
            var R = 6371e3;
            var lat = first.LatitudeLongitude.x * Mathf.Deg2Rad;
            var lat2 = second.LatitudeLongitude.x * Mathf.Deg2Rad;
            var omega = (second.LatitudeLongitude.x - first.LatitudeLongitude.x) * Mathf.Deg2Rad;
            var gamma = (second.LatitudeLongitude.y - first.LatitudeLongitude.y) * Mathf.Deg2Rad;

            var a = Mathf.Sin((float)omega / 2) * Mathf.Sin((float)omega / 2) + Mathf.Cos((float)lat / 2) * Mathf.Cos((float)lat2 / 2) * Mathf.Sin((float)gamma / 2) * Mathf.Sin((float)gamma / 2);
            var c = 2 * Mathf.Atan2(Mathf.Sqrt(a), Mathf.Sqrt(1 - a));
            var dist = R * c;
            var distInMeters = Mathf.Abs(Mathf.RoundToInt((float)dist));
            if (distInMeters > 0)
            {
                distInMeters -= 1;
            }
            return distInMeters;
        }
        else
        {
            return 0;
        }
    }
}
