﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class POISceneLoader : MonoBehaviour
{
    private PlayerMovement player;
    private POISpawner spawner;
    private SceneTransitionManager sceneTransitionManager;
    private GUIManager _GUIManager;

    private string mySpriteName;
    private bool inRange;

    private void Start()
    {
        _GUIManager = FindObjectOfType<GUIManager>();
        inRange = false;
        sceneTransitionManager = FindObjectOfType<SceneTransitionManager>();
        player = FindObjectOfType<PlayerMovement>();
        spawner = FindObjectOfType<POISpawner>();
        StartCoroutine(CheckName());
    }

    #region Coroutines
    private IEnumerator CheckName()
    {
        yield return new WaitForSeconds(1.0f);
        mySpriteName = this.gameObject.name;
        Debug.Log(mySpriteName);
    }
    #endregion

    private void Update()
    {
        if (_GUIManager.IsUIActive)
        {
            return;
        }
        else
        {
            CheckTouch();
        }
    }

    #region InputHandling
    private void CheckTouch()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];
            Ray touchRay = Camera.main.ScreenPointToRay(touch.position);
            RaycastHit hit = new RaycastHit();
            Physics.Raycast(touchRay, out hit);
            if (hit.collider != null && hit.collider.tag.Equals("POI") && inRange)
            {
                if (player.MovSpeed <= player.MaxMovSpeed)
                {
                    //GameManager.Instance.CurrentPlayer.AddXp(bonus);
                    //spawner.Pois[hit.collider.gameObject] = false;
                    LoadSceneBasedOnSpriteName();
                    Destroy(hit.collider.gameObject);
                }
            }
        }
    }


#if UNITY_EDITOR
    private void OnMouseDown()
    {
        if (_GUIManager.IsUIActive)
        {
            return;
        }
        else if (player.MovSpeed <= player.MaxMovSpeed && inRange)
        {
            LoadSceneBasedOnSpriteName();
            Destroy(gameObject);
        }
    }
#endif
    #endregion

    #region Loaders
    private void LoadSceneBasedOnSpriteName()
    {
        if (!mySpriteName.Equals("11"))
        {
            var sceneName = "Objective" + mySpriteName;
            SceneTransitionManager.Instance.GoToScene(sceneName, new List<GameObject>());
        }
        else
        {
            SceneTransitionManager.Instance.GoToScene(PocketDroidsConstants.SCENE_CAPTURE, new List<GameObject>());
        }
    }
    #endregion

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            inRange = true;
            Debug.Log("InRange");
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            inRange = true;
            Debug.Log("InRange");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            inRange = false;
            Debug.Log("OutOfRange");
        }
    }
}
