﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatAndRotate : MonoBehaviour
{
    [SerializeField] private float rotateSpeed = 50;
    [SerializeField] private float floatAmplitude = 2.0f; // Amplitude = ampiezza
    [SerializeField] private float floatFrequency = 1.0f;

    private Vector3 startPos;

    private void Start()
    {
        startPos = transform.position; // Get the object at wich we are attached to position
    }

    void Update()
    {
        transform.Rotate(Vector3.up, rotateSpeed * Time.deltaTime); // Vector3.up indica l'asse y, attorno al quale avverra' la rotazione, rotateSpeed * deltaTime permette di effettuare una rotazione ogni 50s
        Vector3 tmpPos = startPos;
        tmpPos.y += (Mathf.Sin(Time.fixedTime * Mathf.PI * floatFrequency) * floatAmplitude) + 6; // Essendo il seno un sinusoide calcolo il movimento del game object come se seguisse una line sinusoidale
        transform.position = tmpPos;
    }
}
