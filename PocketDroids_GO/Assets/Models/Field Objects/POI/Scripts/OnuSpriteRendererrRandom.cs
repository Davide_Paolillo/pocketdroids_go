﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnuSpriteRendererrRandom : MonoBehaviour
{
    [SerializeField] private List<Sprite> spritesToLoad;

    private SpriteRenderer spriteRenderer;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = spritesToLoad[Random.Range(0, spritesToLoad.Count)];
        this.gameObject.name = spriteRenderer.sprite.name;
    }
}
