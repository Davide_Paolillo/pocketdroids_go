﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class XPBonus : MonoBehaviour
{
    [SerializeField] private int bonus = 10;

    private PlayerMovement player;
    private POISpawner spawner;

    private void Start()
    {
        player = FindObjectOfType<PlayerMovement>();
        spawner = FindObjectOfType<POISpawner>();
    }

    private void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];
            Ray touchRay = Camera.main.ScreenPointToRay(touch.position);
            RaycastHit hit = new RaycastHit();
            Physics.Raycast(touchRay, out hit);
            if (hit.collider != null && hit.collider.tag.Equals("POI"))
            {
                if (player.MovSpeed <= player.MaxMovSpeed)
                {
                    //GameManager.Instance.CurrentPlayer.AddXp(bonus);
                    //spawner.Pois[hit.collider.gameObject] = false;
                    Destroy(hit.collider.gameObject);
                }
            }
        }
    }

    #if UNITY_EDITOR
    private void OnMouseDown()
    {
        if (player.MovSpeed <= player.MaxMovSpeed)
        {
            GameManager.Instance.CurrentPlayer.AddXp(bonus);
            Destroy(gameObject);
        }
    }
    #endif
}
