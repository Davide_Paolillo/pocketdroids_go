﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class POISpawner : Singleton<POISpawner>
{
    private Dictionary<GameObject, bool> pois;

    public Dictionary<GameObject, bool> Pois { get => pois; set => pois = value; }

    void Start()
    {
        pois = new Dictionary<GameObject, bool>();
        foreach(GameObject poi in GameObject.FindGameObjectsWithTag("POI"))
        {
            // retrieve data
            pois.Add(poi, true);
        }
    }

    private void Update()
    {
        
    }

    private void OnApplicationQuit()
    {
        // Save data
    }
}
