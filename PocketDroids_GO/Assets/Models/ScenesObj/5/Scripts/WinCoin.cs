﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinCoin : MonoBehaviour
{
    [SerializeField] private int expToAddForEachCoin = 10;
    [SerializeField] private int coinsNumber = 2;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(this.gameObject);
        ExperienceManager.Instance.UpdateExperience(expToAddForEachCoin * coinsNumber);
        SceneTransitionManager.Instance.GoToScene(PocketDroidsConstants.SCENE_WORLD, new List<GameObject>());
    }
}
