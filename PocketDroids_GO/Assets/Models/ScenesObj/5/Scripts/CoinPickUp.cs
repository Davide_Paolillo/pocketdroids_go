﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPickUp : MonoBehaviour
{
    [SerializeField] private List<GameObject> walls;
    [SerializeField] private GameObject tileWall;
    [SerializeField] private GameObject coinToEnable;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(this.gameObject);
        foreach(GameObject wall in walls)
        {
            wall.SetActive(true);
        }
        tileWall.SetActive(true);
        coinToEnable.SetActive(true);
    }
}
