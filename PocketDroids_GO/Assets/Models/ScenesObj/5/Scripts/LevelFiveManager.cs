﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelFiveManager : MonoBehaviour
{
    [SerializeField] private GameObject player1;
    [SerializeField] private GameObject player2;
    [SerializeField] private GameObject destination1;
    [SerializeField] private GameObject destination2;
    [SerializeField] private List<GameObject> walls;
    [SerializeField] private List<GameObject> girlWalls;
    [SerializeField] private GameObject girlTileWall;
    [SerializeField] private GameObject tileWall;

    private Vector3 player1Pos;
    private Vector3 player2Pos;
    private Boy boy;
    private Girl girl;
    private new Camera camera;
    private Vector2 inputPosOne;
    private Vector2 inputPosTwo;

    private void Start()
    {
        player1Pos = player1.transform.position;
        player2Pos = player2.transform.position;
        boy = FindObjectOfType<Boy>();
        girl = FindObjectOfType<Girl>();
        camera = Camera.main;
        inputPosOne = new Vector2(0, 0);
        inputPosTwo = new Vector2(0, 0);
    }

    private void Update()
    {
#if UNITY_EDITOR
        var inputPos = camera.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetMouseButton(0))
        {
            player1.transform.localScale = new Vector2(Mathf.Abs(player1.transform.localScale.x), player1.transform.localScale.y);
            player1.transform.position = Vector2.MoveTowards(player1.transform.position, destination1.transform.position, 10.0f * Time.deltaTime);
            boy.StartWalk();
        }
        else
        {
            player1.transform.localScale = new Vector2(-1 * Mathf.Abs(player1.transform.localScale.x), player1.transform.localScale.y);
            player1.transform.position = Vector2.MoveTowards(player1.transform.position, player1Pos, 10.0f * Time.deltaTime);
            boy.StartWalk();
        }

        if (Input.GetMouseButton(1))
        {
            player2.transform.localScale = new Vector2(Mathf.Abs(player2.transform.localScale.x), player2.transform.localScale.y);
            player2.transform.position = Vector2.MoveTowards(player2.transform.position, destination2.transform.position, 10.0f * Time.deltaTime);
            girl.StartWalk();
        }
        else
        {
            player2.transform.localScale = new Vector2(-1 * Mathf.Abs(player2.transform.localScale.x), player2.transform.localScale.y);
            player2.transform.position = Vector2.MoveTowards(player2.transform.position, player2Pos, 10.0f * Time.deltaTime);
            girl.StartWalk();
            EnableWalls();
        }

#elif UNITY_ANDROID
        if(Input.touchCount > 0)
        {
            inputPosTwo = camera.ScreenToWorldPoint(Input.touches[0].position);
        }
        else
        {
            inputPosTwo = Vector2.zero;
        }

        if(Input.touchCount > 1)
        {
            inputPosOne = camera.ScreenToWorldPoint(Input.touches[1].position);
        }
        else
        {
            inputPosOne = Vector2.zero;
        }



        if (inputPosTwo.x > 0)
        {
            player2.transform.localScale = new Vector2(Mathf.Abs(player2.transform.localScale.x), player2.transform.localScale.y);
            player2.transform.position = Vector2.MoveTowards(player2.transform.position, destination2.transform.position, 10.0f * Time.deltaTime);
            girl.StartWalk();
        }
        else
        {
            player2.transform.localScale = new Vector2(-1 * Mathf.Abs(player2.transform.localScale.x), player2.transform.localScale.y);
            player2.transform.position = Vector2.MoveTowards(player2.transform.position, player2Pos, 10.0f * Time.deltaTime);
            girl.StartWalk();
            EnableWalls();
        }

        if (inputPosOne.x < 0)
        {
            player1.transform.localScale = new Vector2(Mathf.Abs(player1.transform.localScale.x), player1.transform.localScale.y);
            player1.transform.position = Vector2.MoveTowards(player1.transform.position, destination1.transform.position, 10.0f * Time.deltaTime);
            boy.StartWalk();
        }
        else
        {
            player1.transform.localScale = new Vector2(-1 * Mathf.Abs(player1.transform.localScale.x), player1.transform.localScale.y);
            player1.transform.position = Vector2.MoveTowards(player1.transform.position, player1Pos, 10.0f * Time.deltaTime);
            boy.StartWalk();
        }
#endif

        if (player2.transform.position.x == destination2.transform.position.x)
        {
            girl.StopWalk();
            DisableWalls();
        }
        else if(player2.transform.position.x == player2Pos.x)
        {
            girl.StopWalk();
        }

        if (player1.transform.position.x == destination1.transform.position.x)
        {
            boy.StopWalk();
        }
        else if (player1.transform.position.x == player1Pos.x)
        {
            boy.StopWalk();
            DisableGirlWalls();
        }
    }

    private void DisableWalls()
    {
        foreach (GameObject wall in walls)
        {
            wall.SetActive(false);
        }
        tileWall.SetActive(false);
    }

    private void EnableWalls()
    {
        foreach (GameObject wall in walls)
        {
            wall.SetActive(true);
        }
        tileWall.SetActive(true);
    }

    private void DisableGirlWalls()
    {
        foreach (GameObject wall in girlWalls)
        {
            wall.SetActive(false);
        }
        girlTileWall.SetActive(false);
    }

    private void EnableGirlWalls()
    {
        foreach (GameObject wall in girlWalls)
        {
            wall.SetActive(true);
        }
        girlTileWall.SetActive(true);
    }
}
