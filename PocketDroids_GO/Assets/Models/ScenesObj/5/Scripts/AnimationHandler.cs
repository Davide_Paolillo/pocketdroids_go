﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHandler : MonoBehaviour
{
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void StartWalk()
    {
        animator.SetBool("CanWalk", true);
    }

    public void StopWalk()
    {
        animator.SetBool("CanWalk", false);
    }
}
