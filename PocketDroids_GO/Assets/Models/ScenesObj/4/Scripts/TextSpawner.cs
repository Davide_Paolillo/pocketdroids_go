﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextSpawner : MonoBehaviour
{
    [SerializeField] private List<GameObject> objToSpawn;
    [SerializeField] private float timeToWaitBeforeSpawningAgain = 1.0f;
    [Range(0.0f, 1.0f)][SerializeField] private float randomTimerModifier = 0.0f;
    [SerializeField] private float objTorque = 50.0f;
    [Range(0.0f, 1.0f)] [SerializeField] private float randomTorqueFactor = 0.0f;
    [Range(0.0f, 1.0f)] [SerializeField] private float maxXPosToSpawn = 0.8f;

    private Camera camera;

    private float timerRange;
    private float torqueRange;

    private void Start()
    {
        camera = Camera.main;
        timerRange = timeToWaitBeforeSpawningAgain - (randomTimerModifier * timeToWaitBeforeSpawningAgain);
        torqueRange = objTorque - (randomTorqueFactor * objTorque);
        StartCoroutine(SpawnTimer());
    }

    private IEnumerator SpawnTimer()
    {
        while (true)
        {
            var timer = UnityEngine.Random.Range(timerRange, timeToWaitBeforeSpawningAgain);
            yield return new WaitForSeconds(timer);
            SpawnObjects();
        }
    }

    private void SpawnObjects()
    {
        var randomObj = objToSpawn[UnityEngine.Random.Range(0, objToSpawn.Count)];
        var randomTorque = UnityEngine.Random.Range(torqueRange, objTorque);
        var viewPortRange = camera.ViewportToWorldPoint(new Vector3(maxXPosToSpawn, 0.0f, 0.0f));
        var viewPortMin = camera.ViewportToWorldPoint(new Vector3(0.0f, 0.0f, 0.0f));
        var randomPosition = new Vector3(UnityEngine.Random.Range(viewPortMin.x, viewPortRange.x), this.transform.position.y, this.transform.position.z);
        GameObject obj = Instantiate(randomObj, randomPosition, Quaternion.identity);
        obj.transform.parent = this.transform;
        obj.GetComponent<Rigidbody2D>().AddTorque(randomTorque);
    }

    private void Update()
    {
        timerRange = timeToWaitBeforeSpawningAgain - (randomTimerModifier * timeToWaitBeforeSpawningAgain);
    }
}
