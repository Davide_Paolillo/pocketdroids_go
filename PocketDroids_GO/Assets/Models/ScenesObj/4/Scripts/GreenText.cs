﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenText : MonoBehaviour
{
    [SerializeField] private float xSizeToIncrease = 0.05f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            Destroy(this.gameObject);
            collision.transform.localScale = new Vector3(collision.transform.localScale.x + xSizeToIncrease, collision.transform.localScale.y, collision.transform.localScale.z);
        }
    }
}
