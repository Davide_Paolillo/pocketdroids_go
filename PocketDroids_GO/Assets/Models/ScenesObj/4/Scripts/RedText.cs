﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedText : MonoBehaviour
{
    [SerializeField] private float xSizeToDecrease = 0.2f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            Destroy(this.gameObject);
            collision.transform.localScale = new Vector3(collision.transform.localScale.x - xSizeToDecrease, collision.transform.localScale.y, collision.transform.localScale.z);
        }
    }
}
