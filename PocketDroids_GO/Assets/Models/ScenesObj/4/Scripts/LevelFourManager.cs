﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelFourManager : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private GameObject spawner;
    [SerializeField] private float maxScaleToWin = 1.8f;
    [SerializeField] private float minScaleToLose = 0.1f;

    private bool isRunning;

    private void Start()
    {
        isRunning = false;
    }

    void Update()
    {
        if (player.localScale.x >= maxScaleToWin && isRunning)
        {
            isRunning = true;
            spawner.SetActive(false);
            Debug.Log("YouWin");
            ExperienceManager.Instance.UpdateExperience(Mathf.RoundToInt(player.localScale.x * 10));
            SceneTransitionManager.Instance.GoToScene(PocketDroidsConstants.SCENE_WORLD, new List<GameObject>());
        }
        else if (player.localScale.x <= minScaleToLose && isRunning)
        {
            isRunning = true;
            spawner.SetActive(false);
            Debug.Log("YouLost");
            SceneTransitionManager.Instance.GoToScene(PocketDroidsConstants.SCENE_WORLD, new List<GameObject>());
        }
    }
}
