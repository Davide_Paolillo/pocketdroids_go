﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Book : MonoBehaviour
{
    [SerializeField] private float xRightPadding = 0.2f;
    [SerializeField] private float xLeftPadding = 0.2f;
    [SerializeField] private float movementSpeed = 10.0f;
    [SerializeField] private float filter = 3.0f;

    private Vector3 newPos;
    private Vector3 previousAcceleration;
    private Vector2 cameraXBounds;

    private void Start()
    {
        cameraXBounds = new Vector2(Camera.main.ViewportToWorldPoint(Vector3.zero).x + xLeftPadding, Camera.main.ViewportToWorldPoint(Vector3.one).x - xRightPadding);
        previousAcceleration = new Vector3();
        previousAcceleration = Input.acceleration;
        newPos = new Vector3();
        newPos.y = this.transform.position.y;
        newPos.z = this.transform.position.z;
    }

    private void Update()
    {
#if UNITY_EDITOR

        newPos = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, this.transform.position.y, this.transform.position.z);
        newPos.x = Mathf.Clamp(newPos.x, Camera.main.ViewportToWorldPoint(new Vector2(0, 0)).x + xLeftPadding, Camera.main.ViewportToWorldPoint(new Vector2(1, 0)).x - xRightPadding);
        this.transform.position = newPos;

#elif UNITY_ANDROID

        newPos.x = Mathf.Lerp(previousAcceleration.x, Input.acceleration.x, Time.deltaTime * filter);
        this.transform.Translate(newPos * movementSpeed * Time.deltaTime);
        this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x, cameraXBounds.x, cameraXBounds.y), newPos.y, newPos.z);
#endif

        if (previousAcceleration.x != Input.acceleration.x)
        {
            previousAcceleration = Input.acceleration;
        }
    }
}
