﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricSocket : MonoBehaviour
{
    [SerializeField] private Light light;

    private float initialIntensity;
    private bool isLightDown;
    private bool isLightUp;
    private bool isGameEnded;

    public bool IsGameEnded { get => isGameEnded; }

    private void Start()
    {
        initialIntensity = light.intensity;
        light.intensity = 0;
        isLightDown = false;
        isLightUp = false;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        PlugIn(collision.gameObject);
    }

    private void PlugIn(GameObject collider)
    {
        if (!Input.GetMouseButton(0))
        {
            isLightUp = true;
            collider.GetComponent<Ball>().CanMove = false;
            collider.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            collider.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, collider.transform.position.z);
            StartCoroutine(LightUp());
            //light.intensity = initialIntensity;
        }
    }

    private IEnumerator LightUp()
    {
        while (light.intensity < initialIntensity)
        {
            light.intensity += initialIntensity * Time.deltaTime;
            yield return new WaitForSeconds(0);
        }
        isLightUp = false;
        isGameEnded = true;
        SceneTransitionManager.Instance.GoToScene(PocketDroidsConstants.SCENE_WORLD, new List<GameObject>());
        yield return new WaitForSeconds(0);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (Input.GetMouseButton(0))
        {
            StartCoroutine(lightDown());
        }
    }

    private IEnumerator lightDown()
    {
        if (!isLightUp)
        {
            while (light.intensity > 0)
            {
                light.intensity -= 1.0f * Time.deltaTime;
                yield return new WaitForSeconds(0);
            }
        }
        else
        {
            while (!isLightUp)
            {
                yield return new WaitForSeconds(Mathf.Epsilon);
            }
            while (light.intensity > 0)
            {
                light.intensity -= 1.0f * Time.deltaTime;
                yield return new WaitForSeconds(0);
            }
        }
        yield return new WaitForSeconds(0);
    }
}
