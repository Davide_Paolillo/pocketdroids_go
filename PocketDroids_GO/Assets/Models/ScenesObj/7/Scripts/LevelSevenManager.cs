﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSevenManager : MonoBehaviour
{
    [SerializeField] private ElectricSocket electricSocket;
    [SerializeField] private int xpToAdd = 10;

    private bool isRunning;

    private void Start()
    {
        isRunning = false;
    }

    private void Update()
    {
        if (electricSocket.IsGameEnded && !isRunning)
        {
            isRunning = true;
            ExperienceManager.Instance.UpdateExperience(xpToAdd);
        }
    }
}
