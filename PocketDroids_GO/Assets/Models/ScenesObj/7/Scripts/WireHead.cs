﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WireHead : MonoBehaviour
{
    private bool isGrabbed;
    private bool checkGrab;

    private void Start()
    {
        isGrabbed = false;
        checkGrab = false;
    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            checkGrab = true;
        }
        else
        {
            checkGrab = false;
            isGrabbed = false;
        }
    }

    private void FixedUpdate()
    {
        if (checkGrab)
        {
            var hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.collider != null && hit.collider.tag.Equals("Player"))
            {
                isGrabbed = true;
            }
        }
    }

    public bool isRopeGrabbed()
    {
        return isGrabbed;
    }
}
