﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Rope : MonoBehaviour
{
    [SerializeField] private Rigidbody2D hook;
    [SerializeField] private GameObject linkPrefab;
    [SerializeField] private int links = 7;
    [SerializeField] private LineRenderer lineToRender;
    [SerializeField] private GameObject ball;

    private List<GameObject> connections;

    private float lastPointAngularLimit;

    private void Awake()
    {
        lastPointAngularLimit = 10.0f;
        lineToRender.positionCount = links;
        connections = new List<GameObject>();
        GenerateRope();
    }

    private void GenerateRope()
    {
        Rigidbody2D prefviousRB = hook;
        for(int i = 0; i < links; i++)
        {
            GameObject wire = Instantiate(linkPrefab, transform) as GameObject;
            HingeJoint2D joint = wire.GetComponent<HingeJoint2D>();
            joint.connectedBody = prefviousRB;
            prefviousRB = wire.GetComponent<Rigidbody2D>();
            connections.Add(wire);
        }

        connections[0].GetComponent<HingeJoint2D>().connectedAnchor = new Vector2(0, 0);
    }

    private void Update()
    {
        connections[connections.Count - 1].transform.position = ball.transform.position;
        for (int i = 0; i < connections.Count; i++)
        {
            lineToRender.SetPosition(i, new Vector3(connections[i].transform.position.x, connections[i].transform.position.y, connections[i].transform.position.z));
        }
    }

    public GameObject GetLastRopeComponent()
    {
        return connections[connections.Count - 1];
    }
}
