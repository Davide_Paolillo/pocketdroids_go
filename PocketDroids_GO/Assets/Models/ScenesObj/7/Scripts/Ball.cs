﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] private float timeToWaitBeforeStatic = 5.0f;

    private Rope rope;

    private bool canGrab;
    private bool isRunning;
    private bool canMove;

    public bool CanMove { get => canMove; set => canMove = value; }

    private void Start()
    {
        rope = FindObjectOfType<Rope>();
        this.gameObject.GetComponent<HingeJoint2D>().connectedBody = rope.GetLastRopeComponent().GetComponent<Rigidbody2D>();
        canGrab = false;
        isRunning = true;
        canMove = true;
    }

    private void Update()
    {
        
        canGrab = this.GetComponent<WireHead>().isRopeGrabbed();
#if UNITY_EDITOR
        if (canGrab && canMove)
        {
            if (Input.GetMouseButtonDown(0))
            {
                this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
            }
            else if (Input.GetMouseButton(0))
            {
                this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
                this.transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, this.transform.position.z);
            }
            else if (Input.GetMouseButtonUp(0))
            {
               this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            }
            isRunning = false;
        }
        else
        {
            if (!isRunning)
            {
                StartCoroutine(SetDinamicAfterTimer());
            }
        }
#elif UNITY_ANDROID
        if (canGrab && canMove)
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.touches[0];
                this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
                this.transform.position = new Vector3(Camera.main.ScreenToWorldPoint(touch.position).x, Camera.main.ScreenToWorldPoint(touch.position).y, this.transform.position.z);
            }

            if (Input.touchCount <= 0)
            {
                this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            }
            isRunning = false;
        }
        else
        {
            if (!isRunning)
            {
                StartCoroutine(SetDinamicAfterTimer());
            }
        }
#endif

    }

    private IEnumerator SetDinamicAfterTimer()
    {
        isRunning = true;
        this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        yield return new WaitForSeconds(timeToWaitBeforeStatic);
        if (!canGrab && canMove)
        {
            this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
            this.transform.position = new Vector3(Camera.main.ViewportToWorldPoint(new Vector2(0.5f, 0.8f)).x, Camera.main.ViewportToWorldPoint(new Vector2(0.5f, 0.8f)).y, this.transform.position.z);
        }
    }
}
