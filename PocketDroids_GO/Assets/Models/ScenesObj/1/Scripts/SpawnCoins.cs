﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCoins : MonoBehaviour
{
    [SerializeField] private List<GameObject> coinsToSpawn;
    [SerializeField] private float spawnTimer = 1.0f;
    [SerializeField] private float xLeftMargin = 0.2f;
    [SerializeField] private float xRightMargin = 0.8f;

    private Camera mainCamera;

    private void Start()
    {
        mainCamera = Camera.main;
        StartCoroutine(SpawnCoinsWithTimer());
    }

    private IEnumerator SpawnCoinsWithTimer()
    {
        Vector3 coordinatesToSpawn = new Vector3();
        coordinatesToSpawn.y = this.transform.position.y;
        coordinatesToSpawn.z = mainCamera.nearClipPlane;
        while (true)
        {
            coordinatesToSpawn.x = UnityEngine.Random.Range(xLeftMargin, xRightMargin);
            var coinIndex = UnityEngine.Random.Range(0, coinsToSpawn.Count);
            GameObject coin = Instantiate(coinsToSpawn[coinIndex], new Vector3(mainCamera.ViewportToWorldPoint(coordinatesToSpawn).x, coordinatesToSpawn.y, coordinatesToSpawn.z), Quaternion.identity);
            coin.transform.parent = this.transform;
            yield return new WaitForSeconds(spawnTimer);
        }
    }
}
