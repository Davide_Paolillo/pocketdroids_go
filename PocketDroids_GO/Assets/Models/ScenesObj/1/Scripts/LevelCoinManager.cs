﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LevelCoinManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;

    private int score;

    public int Score { get => score; }

    private void Start()
    {
        score = 0;
        scoreText.text = score.ToString();
    }

    public void UpdateScore(int scoreToadd)
    {
        score += scoreToadd;
        scoreText.text = score.ToString();
    }
}
