﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] private int scoreToadd = 1;

    public int ScoreToadd { get => scoreToadd; }
}
