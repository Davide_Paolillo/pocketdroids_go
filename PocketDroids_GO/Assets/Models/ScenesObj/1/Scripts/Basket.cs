﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Basket : MonoBehaviour
{
    [SerializeField] private float movementSpeed = 20.0f;
    [SerializeField] private float xLeftMargin = 0.2f;
    [SerializeField] private float xRightMargin = 0.2f;
    [SerializeField] private float filter = 3.0f;

    private Rigidbody2D rigidbody;
    private Vector3 movementDirection;
    private Vector3 cameraXBounds;
    private Vector3 previousAcceleration;
    private LevelCoinManager levelCoinManager;


    private void Start()
    {
        levelCoinManager = FindObjectOfType<LevelCoinManager>();
        rigidbody = GetComponent<Rigidbody2D>();
        movementDirection = new Vector3();
        cameraXBounds = new Vector2(Camera.main.ViewportToWorldPoint(Vector3.zero).x + xLeftMargin, Camera.main.ViewportToWorldPoint(Vector3.one).x - xRightMargin);
        movementDirection.y = this.transform.position.y;
        movementDirection.z = this.transform.position.z;
        previousAcceleration = new Vector3();
        previousAcceleration = Vector3.zero;
        //previousAcceleration = Input.acceleration;
    }

    private void Update()
    {

#if UNITY_EDITOR
        this.transform.position = new Vector3(Mathf.Clamp(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, cameraXBounds.x, cameraXBounds.y), movementDirection.y, movementDirection.z);
#elif UNITY_ANDROID

        movementDirection.x = Mathf.Lerp(previousAcceleration.x, Input.acceleration.x, Time.deltaTime * filter);
        this.transform.Translate(movementDirection * movementSpeed * Time.deltaTime);
        this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x, cameraXBounds.x, cameraXBounds.y), movementDirection.y, movementDirection.z);
        //this.transform.position = new Vector3(Mathf.Clamp(movementDirection.x, cameraXBounds.x, cameraXBounds.y), movementDirection.y, movementDirection.z);
#endif
        if (previousAcceleration.x != Input.acceleration.x)
        {
            previousAcceleration = Input.acceleration;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(collision.gameObject);
        levelCoinManager.UpdateScore(collision.gameObject.GetComponent<Coin>().ScoreToadd);
    }
}
