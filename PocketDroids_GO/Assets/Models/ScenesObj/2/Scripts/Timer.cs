﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [SerializeField] private Slider timer;
    [SerializeField] private float levelTimer = 60.0f;
    [SerializeField] private List<GameObject> spawners;
    [SerializeField] private int expToAdd = 1;

    private SceneTransitionManager sceneTransitionManager;

    private bool isGameEnded;
    private bool isRunning;

    private void Start()
    {
        isGameEnded = false;
        isRunning = false;
        timer.maxValue = levelTimer;
        sceneTransitionManager = FindObjectOfType<SceneTransitionManager>();
    }

    private void Update()
    {
        if (!isGameEnded)
        {
            timer.value = Time.timeSinceLevelLoad % levelTimer;
            if (Time.timeSinceLevelLoad >= levelTimer)
            {
                isGameEnded = true;
                if (!isRunning)
                {
                    StartCoroutine(DestroySpawners());
                    if (GetComponent<LevelTwoManager>())
                    {
                        ExperienceManager.Instance.UpdateExperience(expToAdd * GetComponent<LevelTwoManager>().Score);
                    }
                    else if(GetComponent<ScoreUpdater>())
                    {
                        ExperienceManager.Instance.UpdateExperience(expToAdd * GetComponent<ScoreUpdater>().Score);
                    }
                    else
                    {
                        ExperienceManager.Instance.UpdateExperience(Mathf.RoundToInt((expToAdd * GetComponent<LevelCoinManager>().Score) / 2));
                    }
                    isRunning = true;
                    SceneTransitionManager.Instance.GoToScene(PocketDroidsConstants.SCENE_WORLD, new List<GameObject>());
                }
            }
        }
    }

    private IEnumerator DestroySpawners()
    {
        foreach(GameObject spawner in spawners)
        {
            Destroy(spawner);
        }
        yield return new WaitForSeconds(0);
    }
}
