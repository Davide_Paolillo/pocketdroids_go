﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomFoodSelector : MonoBehaviour
{
    [SerializeField] private List<Sprite> sprites;

    private void Start()
    {
        GetComponent<SpriteRenderer>().sprite = sprites[UnityEngine.Random.Range(0, sprites.Count)];
    }
}
