﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject foodToSpawn;
    [SerializeField] private float timeBetweenSpawns = 1.0f;
    [Range(0.0f, 1.0f)] [SerializeField] private float randomTimeFactor = 1.0f;
    [SerializeField] private float yForceToAdd = 100.0f;
    [Range(0.0f, 1.0f)][SerializeField] private float randomYForceFactor = 1.0f;
    [SerializeField] private float xForceToAdd = 100.0f;
    [Range(0.0f, 1.0f)][SerializeField] private float randomXForceFactor = 1.0f;
    [SerializeField] private float torqueToAdd = 100.0f;
    [Range(0.0f, 1.0f)] [SerializeField] private float randomTorqueFactor = 1.0f;


    private float yRandomRangeForces;
    private float xRandomRangeForces;
    private float timeRandomRange;
    private float torqueRandomRange;

    void Start()
    {
        timeRandomRange = (timeBetweenSpawns - (timeBetweenSpawns * randomTimeFactor)) <= 0 ? 0 : timeBetweenSpawns - (timeBetweenSpawns * randomTimeFactor);
        yRandomRangeForces = yForceToAdd - (yForceToAdd * randomYForceFactor);
        xRandomRangeForces = xForceToAdd - (xForceToAdd * randomXForceFactor);
        torqueRandomRange = torqueToAdd - (torqueToAdd * randomTorqueFactor);
        StartCoroutine(SpawnFoodEveryTime());
    }

    private IEnumerator SpawnFoodEveryTime()
    {
        Vector2 forcesToAdd = new Vector2();
        while (true)
        {
            GameObject food = Instantiate(foodToSpawn, this.transform.position, Quaternion.identity);
            forcesToAdd.x = UnityEngine.Random.Range(xRandomRangeForces, xForceToAdd);
            forcesToAdd.x *= UnityEngine.Random.Range(0.0f, 1.0f) > 0.4f ? -1 : 1;
            forcesToAdd.y = UnityEngine.Random.Range(yRandomRangeForces, yForceToAdd);
            food.GetComponent<Rigidbody2D>().AddForce(forcesToAdd);
            food.GetComponent<Rigidbody2D>().AddTorque(UnityEngine.Random.Range(torqueRandomRange, torqueToAdd));
            food.transform.parent = this.transform;
            yield return new WaitForSeconds(UnityEngine.Random.Range(timeRandomRange, timeBetweenSpawns));
        }
    }
}
