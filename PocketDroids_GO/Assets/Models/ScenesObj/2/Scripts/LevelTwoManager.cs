﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LevelTwoManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private int scoreToAdd = 1;

    private int score;

    public int Score { get => score; }

    private void Start()
    {
        score = 0;
        scoreText.text = score.ToString();
    }

    private void Update()
    {
        #region PlatformDependentCompilation
#if UNITY_EDITOR
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Input.GetMouseButtonDown(0))
        {
            var hit = Physics2D.Raycast(ray.origin, ray.direction);
            if (hit.collider != null && hit.collider.gameObject.tag.Equals("Fruit"))
            {
                Destroy(hit.collider.gameObject);
                UpdateScore(scoreToAdd);
            }
        }
#endif
        #endregion
        if(Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];
            Ray touchRay = Camera.main.ScreenPointToRay(touch.position);
            if (touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Stationary)
            {
                var hit = Physics2D.Raycast(touchRay.origin, touchRay.direction);
                if (hit.collider != null && hit.collider.gameObject.tag.Equals("Fruit"))
                {
                    Destroy(hit.collider.gameObject);
                    UpdateScore(scoreToAdd);
                }
            }
        }
    }

    private void UpdateScore(int scoreToAdd)
    {
        score += scoreToAdd;
        scoreText.text = score.ToString();
    }
}
