﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyPlastic : MonoBehaviour
{
    [SerializeField] private int scoreToAdd = 1;

    private ScoreUpdater scoreUpdater;

    private void Start()
    {
        scoreUpdater = FindObjectOfType<ScoreUpdater>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(collision.gameObject);
        scoreUpdater.UpdateScore(scoreToAdd);
    }
}
