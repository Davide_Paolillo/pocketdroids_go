﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottleSpawner : MonoBehaviour
{
    [SerializeField] private List<Sprite> spritesToSpawn;
    [SerializeField] private GameObject objToSpawn;
    [SerializeField] private float spawnTimer = 1.0f;
    [SerializeField] private float xLeftMargin = 0.2f;
    [SerializeField] private float xRightMargin = 0.8f;
    [SerializeField] private float torqueMaxForce = 100.0f;

    private Camera mainCamera;

    private void Start()
    {
        mainCamera = Camera.main;
        StartCoroutine(SpawnCoinsWithTimer());
    }

    private IEnumerator SpawnCoinsWithTimer()
    {
        Vector3 coordinatesToSpawn = new Vector3();
        coordinatesToSpawn.y = this.transform.position.y;
        coordinatesToSpawn.z = mainCamera.nearClipPlane;
        while (true)
        {
            coordinatesToSpawn.x = UnityEngine.Random.Range(xLeftMargin, xRightMargin);
            var spriteIndex = UnityEngine.Random.Range(0, spritesToSpawn.Count);
            objToSpawn.GetComponent<SpriteRenderer>().sprite = spritesToSpawn[spriteIndex];
            GameObject obj = Instantiate(objToSpawn, new Vector3(mainCamera.ViewportToWorldPoint(coordinatesToSpawn).x, coordinatesToSpawn.y, coordinatesToSpawn.z), Quaternion.identity);
            obj.GetComponent<Rigidbody2D>().AddTorque(UnityEngine.Random.Range(0.0f, torqueMaxForce));
            obj.transform.parent = this.transform;
            yield return new WaitForSeconds(spawnTimer);
        }
    }
}
