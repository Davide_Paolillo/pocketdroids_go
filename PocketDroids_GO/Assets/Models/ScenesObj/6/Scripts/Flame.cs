﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flame : MonoBehaviour
{
    [SerializeField] private GameObject flame;

    // Start is called before the first frame update
    void Start()
    {
        flame.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
        {
            flame.SetActive(true);
            flame.transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, flame.transform.position.z);
        }
        else
        {
            flame.SetActive(false);
        }


#elif UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];
            flame.SetActive(true);
            flame.transform.position = new Vector3(Camera.main.ScreenToWorldPoint(touch.position).x, Camera.main.ScreenToWorldPoint(touch.position).y, flame.transform.position.z);
        }
        else
        {
            flame.SetActive(false);
        }
#endif
    }
}
