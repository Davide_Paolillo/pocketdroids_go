﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudParallax : Parallax
{
    [SerializeField] private GameObject objToParallax;
    [SerializeField] private bool shouldPrewarm = false;
    [SerializeField] private float yRandomFactor = 1.0f;
    [SerializeField] private Transform rightMargin;

    private Vector2 coordinatesToSpawn;
    private SpriteRenderer spriteRenderer;
    private BoxCollider2D collider;

    private float screenAspectRation;
    private float screenWidth;
    private bool canSpawn;

    void Start()
    {
        canSpawn = true;
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.flipX = UnityEngine.Random.Range(0.0f, 1.0f) > 0.5f ? false : true;
        screenAspectRation = Screen.width / Screen.height;
        screenWidth = (Camera.main.orthographicSize * 2) * 2;
        collider = GetComponent<BoxCollider2D>();
        coordinatesToSpawn = new Vector2(rightMargin.position.x + (collider.bounds.extents.x / 1.2f), UnityEngine.Random.Range(this.transform.position.y - yRandomFactor, this.transform.position.y + yRandomFactor));
        if (shouldPrewarm)
        {
            PrewarmParallax();
        }
    }

    private IEnumerator StartParallaxing()
    {
        while (this.transform.parent.childCount >= NumberOfObjectToPrewarm)
        {
            yield return new WaitForSeconds(0);
        }
        coordinatesToSpawn = new Vector2(rightMargin.position.x + collider.bounds.extents.x, UnityEngine.Random.Range(this.transform.position.y - yRandomFactor, this.transform.position.y + yRandomFactor));
        ParallaxEffect();
    }

    protected override void PrewarmParallax()
    {
        ParallaxEffect();
    }

    protected override void ParallaxEffect()
    {
        if (this.transform.parent.childCount < NumberOfObjectToPrewarm)
        {
            GameObject cloud = Instantiate(objToParallax, coordinatesToSpawn, Quaternion.identity) as GameObject;
            cloud.transform.parent = this.transform.parent;
            cloud.GetComponent<CloudParallax>().myInitialScrollingSpeed = this.myInitialScrollingSpeed;
        }
        else if (this.transform.parent.childCount >= NumberOfObjectToPrewarm)
        {
            canSpawn = false;
            StartCoroutine(StartParallaxing());
        }
    }

    protected override void CheckIfOutOfRange()
    {
        base.CheckIfOutOfRange();
        if (rightMargin.position.x < (screenWidth * -1))
        {
            Destroy(this.gameObject);
        }
    }
}
