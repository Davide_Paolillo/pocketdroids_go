﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private float timeToWaitBeforeStartingLevel = 3.0f;
    [SerializeField] private TextMeshProUGUI countdownText;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private PlayerAnimationHandler playeAnimations;

    private float oneSecond;
    private bool disableInput;
    private int score;

    public bool DisableInput { get => disableInput; set => disableInput = value; }
    public int Score { get => score; }

    private void Start()
    {
        score = 0;
        scoreText.text = score.ToString();
        scoreText.gameObject.SetActive(false);
        disableInput = true;
        SetParallaxSpeedToZero();
        oneSecond = 1.0f;
        StartCoroutine(Countdown());
    }

    private void SetParallaxSpeedToZero()
    {
        foreach (Parallax parallax in FindObjectsOfType<Parallax>())
        {
            parallax.NullScrollingSpeed();
        }
    }

    private IEnumerator Countdown()
    {
        float counter = 0.0f;
        while(counter < timeToWaitBeforeStartingLevel)
        {
            countdownText.text = Mathf.RoundToInt(timeToWaitBeforeStartingLevel - counter).ToString();
            yield return new WaitForSeconds(oneSecond);
            ++counter;
        }
        countdownText.text = Mathf.RoundToInt(timeToWaitBeforeStartingLevel - counter).ToString();
        scoreText.gameObject.SetActive(true);
        StartCoroutine(DisableCountdownText());
        ResetParallaxSpeed();
        playeAnimations.PlayerStartRun();
        disableInput = false;
    }

    private IEnumerator DisableCountdownText()
    {
        yield return new WaitForSeconds(0.5f);
        countdownText.gameObject.SetActive(false);
    }

    private void ResetParallaxSpeed()
    {
        foreach (Parallax parallax in FindObjectsOfType<Parallax>())
        {
            parallax.ResetScrollingSpeed();
        }
    }

    public void UpdateScore(int pointsToAdd)
    {
        score += pointsToAdd;
        scoreText.text = score.ToString();
    }
}
