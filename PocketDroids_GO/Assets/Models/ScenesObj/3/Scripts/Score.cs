﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    [SerializeField] private LevelManager levelManager;
    [SerializeField] private int scoreToAdd = 1;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            Destroy(this.gameObject);
            levelManager.UpdateScore(scoreToAdd);
        }
    }
}
