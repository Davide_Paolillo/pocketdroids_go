﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
    [SerializeField] private float jumpForce = 200.0f;
    [SerializeField] private float lowJumpGravityMultiplier = 3.0f;
    [SerializeField] private float fallJumpGravityMultiplier = 2.5f;
    [SerializeField] private LevelManager levelManager;

    private PlayerAnimationHandler playerAnimationHandler;
    private Rigidbody2D player;

    private bool jump;

    // Start is called before the first frame update
    void Start()
    {
        playerAnimationHandler = GetComponent<PlayerAnimationHandler>();
        player = GetComponent<Rigidbody2D>();
        jump = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!levelManager.DisableInput)
        {
            #region PlatformDefendentCompilations
#if UNITY_EDITOR
            jump = Input.GetMouseButton(0);
#endif
            #endregion

            if (Input.touchCount > 0)
            {
                Touch touch = Input.touches[0];
                jump = touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Began ? true : false;
            }

        }
    }

    private void FixedUpdate()
    {
        if (jump && playerAnimationHandler.IsTouchingGround && player.velocity.y < 1)
        {
            Jump();
        }
        else if (!jump && !playerAnimationHandler.IsTouchingGround && player.velocity.y > 0)
        {
            player.velocity += Vector2.up * Physics2D.gravity * lowJumpGravityMultiplier * Time.fixedDeltaTime; // increasing gravity if we stop pressing
        }
        else if (!playerAnimationHandler.IsTouchingGround && player.velocity.y < 0)
        {
            player.velocity += Vector2.up * Physics2D.gravity * fallJumpGravityMultiplier * Time.fixedDeltaTime; // increasing gravity when falling
        }
    }

    private void Jump()
    {
        player.AddForce(new Vector2(0, jumpForce));
        playerAnimationHandler.PlayerJump();
    }
}
