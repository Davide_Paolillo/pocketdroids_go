﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrel : MonoBehaviour
{
    private LevelManager levelManager;

    private float delayToLoadWorldScene;

    private void Start()
    {
        delayToLoadWorldScene = 3.0f;
        levelManager = FindObjectOfType<LevelManager>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            Debug.Log("Lost");
            Destroy(collision.gameObject);
            levelManager.DisableInput = true;
            StartCoroutine(LoadWorldSceneDelayed());
            foreach (Parallax parallax in FindObjectsOfType<Parallax>())
            {
                parallax.NullScrollingSpeed();
            }
        }
    }

    private IEnumerator LoadWorldSceneDelayed()
    {
        ExperienceManager.Instance.UpdateExperience(levelManager.Score);
        yield return new WaitForSeconds(delayToLoadWorldScene);
        SceneTransitionManager.Instance.GoToScene(PocketDroidsConstants.SCENE_WORLD, new List<GameObject>());
    }
}
