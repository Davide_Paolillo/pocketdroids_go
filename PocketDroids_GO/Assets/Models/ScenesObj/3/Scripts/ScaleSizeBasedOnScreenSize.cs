﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleSizeBasedOnScreenSize : MonoBehaviour
{
    private int screenAspectRatio;
    private float screenWidth;
    private int initialAspectRatio;

    void Start()
    {
        screenAspectRatio = (Screen.width / Screen.height);
        initialAspectRatio = screenAspectRatio;
        screenWidth = (Camera.main.orthographicSize * 2) * screenAspectRatio;
        this.transform.localScale = new Vector3(screenAspectRatio - this.transform.localScale.x, screenAspectRatio - this.transform.localScale.y, 1);
    }

    private void Update()
    {
        screenAspectRatio = (Screen.width / Screen.height);
        if(initialAspectRatio != screenAspectRatio)
        {
            initialAspectRatio = screenAspectRatio;
            this.transform.localScale = new Vector3(screenAspectRatio, screenAspectRatio, 1);
        }
    }
}
