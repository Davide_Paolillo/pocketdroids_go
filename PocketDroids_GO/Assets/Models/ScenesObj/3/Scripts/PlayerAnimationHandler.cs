﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationHandler : MonoBehaviour
{
    [SerializeField] private Animator animator;

    private Rigidbody2D player;

    private bool isTouchingGround;

    public bool IsTouchingGround { get => isTouchingGround; }

    private void Start()
    {
        player = GetComponent<Rigidbody2D>();
        isTouchingGround = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Ground"))
        {
            animator.SetBool("IsTouchingGround", true);
            isTouchingGround = true;
        }
    }

    private void Update()
    {
        if (player.velocity.y != 0.0f)
        {
            animator.SetBool("IsTouchingGround", false);
            isTouchingGround = false;
        }
    }

    public void PlayerStartRun()
    {
        animator.SetBool("CanRun", true);
    }

    public void PlayerJump()
    {
        animator.SetTrigger("Jump");
    }
}
