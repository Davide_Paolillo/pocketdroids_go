﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBarrels : MonoBehaviour
{
    [SerializeField] private List<GameObject> barrels;

    private void Awake()
    {
        foreach (GameObject barrel in barrels)
        {
            barrel.SetActive(false);
        }
    }

    private void Start()
    {
        foreach (GameObject barrel in barrels)
        {
            barrel.SetActive(UnityEngine.Random.Range(0, 10) > 4 ? true : false);
        }
    }
}
