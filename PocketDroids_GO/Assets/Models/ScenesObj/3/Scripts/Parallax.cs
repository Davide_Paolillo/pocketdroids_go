﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Parallax : MonoBehaviour
{
    [SerializeField] private float scrollingSpeed = 5.0f;
    [SerializeField] private int numberOfObjectToPrewarm = 5;

    protected float myInitialScrollingSpeed;

    public float ScrollingSpeed { get => scrollingSpeed; }
    public int NumberOfObjectToPrewarm { get => numberOfObjectToPrewarm; }

    protected abstract void ParallaxEffect();
    protected virtual void PrewarmParallax() { }

    private void Awake()
    {
        myInitialScrollingSpeed = scrollingSpeed > 0 ? scrollingSpeed : 1.0f;
    }

    private void Update()
    {
        ParallaxScroll();
        CheckIfOutOfRange();
    }

    protected virtual void CheckIfOutOfRange() {}

    protected void ParallaxScroll()
    {
        this.transform.Translate(Vector2.left * Time.deltaTime * scrollingSpeed);
    }

    public void NullScrollingSpeed()
    {
        scrollingSpeed = 0.0f;
    }

    public void ResetScrollingSpeed()
    {
        scrollingSpeed = myInitialScrollingSpeed;
    }
}
