﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class OverrideOrb : MonoBehaviour
{
    [SerializeField] private float throwSpeed = 40.0f; // Speed at wich the ball will travel
    [SerializeField] private float stallTime = 20.0f; // The time that we survive if the ball doesn't collide with something
    [SerializeField] private AudioClip dropSound;
    [SerializeField] private AudioClip successSound;
    [SerializeField] private AudioClip throwSound;

    private Rigidbody rigidbody;
    private AudioSource audioSource;
    private InputStatus inputStatus;
    private CaptureSceneManager captureSceneManager;

    private float lastXposition;
    private float lastYposition;
    private bool released;
    private bool holding;
    private bool trackingCollisions;

    private enum InputStatus
    {
        Grabbing,
        Holding,
        Releasing,
        None
    }

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        rigidbody = GetComponent<Rigidbody>();
        captureSceneManager = FindObjectOfType<CaptureSceneManager>();
        audioSource.playOnAwake = false;
        rigidbody.useGravity = false;
        trackingCollisions = false;

        Assert.IsNotNull(audioSource);
        Assert.IsNotNull(rigidbody);
        Assert.IsNotNull(dropSound);
        Assert.IsNotNull(successSound);
        Assert.IsNotNull(throwSound);
    }

    // Update is called once per frame
    void Update()
    {
        if (released)
        {
            return;
        }

        if (holding)
        {
            FollowInput();
        }

       UpdateInputStatus();
        
        switch (inputStatus)
        {
            case InputStatus.Grabbing:
               Grab();
                Debug.Log("Grab");
                break;
            case InputStatus.Holding:
                Drag();
                Debug.Log("Drag");
                break;
            case InputStatus.Releasing:
                Release();
                Debug.Log("Releasing");
                break;
            case InputStatus.None:
                Debug.Log("None");
                return;
            default:
                return;
        }
    }

    private void Release()
    {
        if (lastYposition < GetInputPosition().y)
        {
            Throw(GetInputPosition()); // check that the user is throwing the ball upwards and not backwords, so we prevent some bugs
        }
    }

    private void Throw(Vector2 targetPos)
    {
        rigidbody.useGravity = true; // The rigidbody wich we are attached to
        trackingCollisions = true;

        float yDiff = (targetPos.y - lastYposition) / Screen.height * 100; // Guarda l'ultima e la prima y per sapere quanta distanza c'e' tra la y di inizio lancio e la y finale di lancio, 
                                                                           // dividendo per la altezza dello schermo * 100, in modo da rendere valori 0,12.. valori interi paragonabili alle y
                                                                           // facendo la divisione so, in base a quanto lo schermo e' grande, se l'utente ha compiuto un lancio lungo o corto
        float speed = throwSpeed * yDiff; // In questo modo moltiplico la velocita' che avra' la palla in base al tipo di lancio, se sara' corto, allora la palla andra' piu' piano, se sara' lungo allora andra' piu' forte

        float xDiff = (targetPos.x - lastXposition) / Screen.width; // Differenza tra ultima x e x iniziale, divisa per la grandezza dello schermo (fattore con cui la x e' cambiata, vogliamo capire se vuole andare a dx o sx)
        xDiff = Mathf.Abs(targetPos.x - lastXposition) / Screen.width * 100 * xDiff; // Prendo il valore assoluto della differenza tra la x attuale e l'ultima x, lo divido per lo schermo scalato a valori interi,
                                                                                     // moltiplicato per la xDiff precedentemente calcolata (cioe'la grandezza dello schermo in valori interi, moltiplicata per il fattore di cambio x)
                                                                                     // in poche parole calcoliamo a quale angolo la palla viene lanciata a dx o sx
        Vector3 direction = new Vector3(xDiff, 0.0f, 1.0f);
        direction = Camera.main.transform.TransformDirection(direction); // La direzione (dx o sx) in cui ci muoviamo adattata ai parametri del mondo tramite funzioni di libreria della camera

        rigidbody.AddForce((direction * speed / 2.0f) + Vector3.up * speed); // Abbiamo aggiunto della forza alla direzione in cui andiamo, in modo di tirare la sfera nella direzione in cui vogliamo

        audioSource.PlayOneShot(throwSound);
        released = true;
        holding = false;

        StartCoroutine(PowerDown()); // Chiamo il metodo PowerDown dopo che stallTime si e' esaurito, questo fa in modo che se non prendo nulla dopo "stallTime" (5s) dal lancio, la sfera viene distrutta
    }

    // Updating positions
    private void Drag()
    {
        lastXposition = GetInputPosition().x;
        lastYposition = GetInputPosition().y;
    }

    private void Grab()
    {
        Ray ray = Camera.main.ScreenPointToRay(GetInputPosition()); // A ray (raggio laser) that goes from the middle of the camera through the point that we indicate in his field
        RaycastHit point; // Used to  get informations basck from a raycast (See what we are touching)
        
        if(Physics.Raycast(ray, out point, 100.0f) /* Our output of Physics.Raycast will be stored inside our variable point, 100.0f is the max distance where we search for an obj */
           && point.transform == transform /* If the the rigidbody that we hit is the rigidbody that we are looking for (so in this case the obj that is attached to our script) */)
        {
            holding = true;
            transform.parent = null;
        }
    }

    // Platform dependent compilation function
    private void UpdateInputStatus()
    {
        // Platform dependent compilation, allows you to run specific code based on the platform where the program is currently running
        #if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            inputStatus = InputStatus.Grabbing;
        } else if(Input.GetMouseButton(0))
        {
            inputStatus = InputStatus.Holding;
        } else if (Input.GetMouseButtonUp(0))
        {
            inputStatus = InputStatus.Releasing;
        } else
        {
            inputStatus = InputStatus.None;
        }
        #endif
        // We use not unity editor cos we will release the app only for touch mobile phones, so we don't need to handle anything else then mobile touch screen,
        //the editor is not giving care of this cos he cares only for the current code that will run, so this is like commented as soon as we run it on a PC
        if (Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];
            if (touch.phase == TouchPhase.Began)
            {
                inputStatus = InputStatus.Grabbing;
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                inputStatus = InputStatus.Releasing;
            }
            else if (Input.touchCount == 1)
            {
                inputStatus = InputStatus.Holding;
            }
            else
            {
                inputStatus = InputStatus.None;
            }
        }
        /*#if NOT_UNITY_EDITOR
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                inputStatus = InputStatus.Grabbing;
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                inputStatus = InputStatus.Releasing;
            }
            else if (Input.touchCount == 1)
            {
                inputStatus = InputStatus.Holding;
            }
            else
            {
                inputStatus = InputStatus.None;
            }
        }
        #endif*/
    }

    // Follow the movement of the player touch/mosue with the orb grabbed
    private void FollowInput()
    {
        Vector3 inputPos = GetInputPosition();
        inputPos.z = Camera.main.nearClipPlane *  7.5f; // nearClipPlane indica il punto relativo piu' vicino alla camera da cui si iniziera' a renderizzare, farClipPlane invece indica il punto piu' lontano in cui la camera renderizzera'
        Vector3 pos = Camera.main.ScreenToWorldPoint(inputPos); // inputPos -> A screen space position (often mouse x, y), plus a z position for depth (for example, a camera clipping plane), returns The worldspace point 
                                                                // created by converting the screen space point at the provided distance z from the camera plane. (ritorna il punto nel mondo alla profondita' z (inputPos.z) dalla camera)
        pos.y -= 0.3f;
        pos.x -= 0.3f;
        transform.localPosition = Vector3.Lerp(transform.localPosition, pos, 50.0f * Time.deltaTime); // Lerp "Lineary interpolates between two vectors" this allow us to smooth the movement between two positions in the screen
    }

    // Returns the position of the mouse/finger
    private Vector2 GetInputPosition()
    {
        Vector2 result = new Vector2();
        
        #if UNITY_EDITOR
        result = Input.mousePosition;
        #endif
        if (Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];
            result = touch.position;
        }
        /*#if NOT_UNITY_EDITOR
        result = Input.GetTouch(0).position;
        #endif*/
        return result;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!trackingCollisions)
        {
            return;
        }

        trackingCollisions = false;

        StartCoroutine(PowerDown());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals(PocketDroidsConstants.TAG_DROID))
        {
            captureSceneManager.BinCentered();
        }
    }

    private IEnumerator PowerDown()
    {
        yield return new WaitForSeconds(stallTime);
        CaptureSceneManager manager = FindObjectOfType<CaptureSceneManager>();
        if (manager != null)
        {
            manager.OrbDestroyed();
        }
        Destroy(gameObject);
    }
}
