﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WrongAnswer : Question
{
    protected override bool MyAnswer()
    {
        return false;
    }
}
