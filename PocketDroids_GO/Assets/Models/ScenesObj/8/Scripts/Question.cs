﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public abstract class Question : MonoBehaviour
{
    [SerializeField] private int expToAdd = 10;

    private EventSystem eventSystem;
    private StandaloneInputModule standaloneInputModule;

    private void Start()
    {
        eventSystem = FindObjectOfType<EventSystem>();
        standaloneInputModule = FindObjectOfType<StandaloneInputModule>();
    }

    protected abstract bool MyAnswer();

    public void OnAnswerClick()
    {
        Destroy(standaloneInputModule);
        Destroy(eventSystem);
        if (MyAnswer())
        {
            Debug.Log("correct answer");
            StartCoroutine(WinCoroutine());
        }
        else
        {
            Debug.Log("wrong answer");
            StartCoroutine(LoseCoroutine());
        }
    }

    private IEnumerator LoseCoroutine()
    {
        GetComponent<Image>().color = Color.red;
        yield return new WaitForSeconds(3.0f);
        SceneTransitionManager.Instance.GoToScene(PocketDroidsConstants.SCENE_WORLD, new List<GameObject>());
    }

    private IEnumerator WinCoroutine()
    {
        GetComponent<Image>().color = Color.green;
        ExperienceManager.Instance.UpdateExperience(expToAdd);
        yield return new WaitForSeconds(3.0f);
        SceneTransitionManager.Instance.GoToScene(PocketDroidsConstants.SCENE_WORLD, new List<GameObject>());
    }
}
