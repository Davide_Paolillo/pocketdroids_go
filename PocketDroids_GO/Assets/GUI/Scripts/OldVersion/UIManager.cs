﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Text xpText;
    [SerializeField] private Text lvlText;
    [SerializeField] private GameObject menu;
    [SerializeField] private AudioClip menuBtnSound;

    private AudioSource audioSource;

    // Called at the start only if the scritp istance is enabled
    public void Start()
    {
        menu.SetActive(false);
    }

    // Called before any Start function, even if the script istance is not enabled
    private void Awake()
    {
        // Initialization of our audio source
        audioSource = GetComponent<AudioSource>();
        // Assertions (check) for null values
        Assert.IsNotNull(audioSource);
        Assert.IsNotNull(xpText);
        Assert.IsNotNull(lvlText);
        Assert.IsNotNull(menu);
        Assert.IsNotNull(menuBtnSound);
    }

    private void Update()
    {
        UpdateLvlText();
        UpdateXPText();
    }

    public void UpdateLvlText()
    {
        lvlText.text = GameManager.Instance.CurrentPlayer.Lvl.ToString();
    }

    public void UpdateXPText()
    {
        xpText.text = GameManager.Instance.CurrentPlayer.Xp + " / " + GameManager.Instance.CurrentPlayer.RequiredXp;
    }

    public void MenuBtnClicked()
    {
        audioSource.PlayOneShot(menuBtnSound);
        ToggleMenu();
    }

    private void ToggleMenu()
    {
        menu.SetActive(!menu.activeSelf);
    }
}
