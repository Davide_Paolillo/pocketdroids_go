﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIManager : MonoBehaviour
{
    [SerializeField] private GameObject menu;

    private bool isUIActive;

    public bool IsUIActive { get => isUIActive; }

    private void Start()
    {
        isUIActive = false;
        menu.transform.localScale = Vector3.zero;
    }

    public void OnMenuIconClick()
    {
        if (menu.transform.localScale != Vector3.zero)
        {
            isUIActive = false;
            menu.transform.localScale = Vector3.zero;
            //menu.SetActive(false);
        }
        else
        {
            isUIActive = true;
            //menu.SetActive(true);
            menu.transform.localScale = Vector3.one;
        }
    }
}
