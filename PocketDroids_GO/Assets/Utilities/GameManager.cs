﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class GameManager : Singleton<GameManager>
{
    private Player currentPlayer; // With the singleton we can retrieve our player data and use it also in other scenes

    public Player CurrentPlayer
    {
        get
        {
            // If the player is not set in the current scene, we create a new one, so we don't need to attach the Player script to a game obj,
            // cos the game manager will handle everything
            if(currentPlayer == null)
            {
                currentPlayer = gameObject.AddComponent<Player>();
            }
            return currentPlayer;
        }
    }

    public void StartPause(float timeToPause)
    {
        //StartCoroutine(StartPauseWithTimer(timeToPause));
    }

    private IEnumerator StartPauseWithTimer(float timeToPause)
    {
        Time.timeScale = 0.0f;
        yield return new WaitForSeconds(timeToPause);
        Time.timeScale = 1.0f;
    }
}
