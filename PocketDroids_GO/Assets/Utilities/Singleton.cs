﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Class wich allow us to ensure there is only one instance of a class for each game session,
/// so we can avoid for example bugs where in a single player game we control 2 players or more.
/// </summary>
public abstract class Singleton<T> : MonoBehaviour where T: MonoBehaviour // This means that also the Key Type T will inherit from MonoBehaviour
{
    private static T instance; // That is the gateway that allow us to access the class T

    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<T>();
            }
            DontDestroyOnLoad(instance); // This allow us to keep the T object data in memory, even if we change scene, so we can retrieve the data that we need in a simple way
            return instance;
        }
    }
}
