﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour
{
    [SerializeField] private GameObject menu;
    [SerializeField] private GameObject hamburgerMenu;

    private Vector3 initialLocalScale;

    private void Start()
    {
        initialLocalScale = hamburgerMenu.transform.localScale;
        if (FindObjectsOfType<Loader>().Length > 1)
        {
            Destroy(this.gameObject);
        }
    }

    private void Update()
    {
        if(SceneManager.GetActiveScene().buildIndex != 0)
        {
            menu.transform.localScale = Vector3.zero;
            hamburgerMenu.transform.localScale = Vector3.zero;
        }
        else
        {
            hamburgerMenu.transform.localScale = initialLocalScale;
        }
    }
}
