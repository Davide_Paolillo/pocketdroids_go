﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PocketDroidsSceneManager : MonoBehaviour
{
    public abstract void playerTapped(GameObject player);
    public abstract void droidTapped(GameObject droid);
    public virtual void droidCollision(GameObject droid, Collision collision) {} // Virtual e' come il membro di una interfaccia, non c'e' bisogno di reimplementare il metodo in tutte le classi che estendono qeusta classe astratta
                                                                                 // ma si puo' fare override solo all' occorrenza
}
