﻿using Michsky.UI.ModernUIPack;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ExperienceManager : Singleton<ExperienceManager>
{
    [SerializeField] private ProgressBar expSlider;
    [SerializeField] private TextMeshProUGUI lvlText;

    private RadialSlider currentSlider;

    private float exp;
    private float level;

    public float Exp { get => exp; }
    public float Level { get => level; }

    private void Start()
    {
        level = GameManager.Instance.CurrentPlayer.Lvl;
        CalculateAndShowExpPercentage();
        lvlText.text = level.ToString();
    }

    private void Update()
    {
        if (level != GameManager.Instance.CurrentPlayer.Lvl)
        {
            level = GameManager.Instance.CurrentPlayer.Lvl;
            lvlText.text = level.ToString();
        }

        CalculateAndShowExpPercentage();
    }

    public void CalculateAndShowExpPercentage()
    {
        exp = GameManager.Instance.CurrentPlayer.Xp;
        expSlider.currentPercent = ((exp * 100) / GameManager.Instance.CurrentPlayer.RequiredXp);
    }

    public void UpdateExperience(int value)
    {
        GameManager.Instance.CurrentPlayer.AddXp(value);
        CalculateAndShowExpPercentage();
    }

    public void ResetAllTheSavedData()
    {
        GameManager.Instance.CurrentPlayer.ResetData();
    }
}
