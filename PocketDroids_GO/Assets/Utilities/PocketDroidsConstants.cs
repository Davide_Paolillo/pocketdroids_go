﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PocketDroidsConstants
{
    public static string[] scenes = { "Objective1", "Objective2", "Objective3", "Objective4", "Objective5", "Objective6", "Objective7", "Capture" };
    public static string SCENE_WORLD = "World";
    public static string SCENE_CAPTURE = "Capture";
    public static string SCENE_3 = "Objective3";
    public static string SCENE_2 = "Objective2";
    public static string SCENE_1 = "Objective1";
    public static string SCENE_5 = "Objective5";
    public static string SCENE_4 = "Objective4";
    public static string SCENE_6 = "Objective6";
    public static string SCENE_7 = "Objective7";
    public static string TAG_DROID = "Droid";
    public static string TAG_OVERRIDE_ORB = "OverrideOrb";
}
