﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeTransitionManager : MonoBehaviour
{
    [SerializeField] private GameObject fadeInCanvas;
    [SerializeField] private GameObject fadeOutCanvas;

    private Animator animator;

    public GameObject InstantiateNewFadeInCanvas()
    {
        GameObject transitionCanvas = Instantiate(fadeInCanvas, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        return transitionCanvas;
    }

    public GameObject InstantiateNewFadeOutCanvas()
    {
        GameObject transitionCanvas = Instantiate(fadeOutCanvas, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        return transitionCanvas;
    }

    public void PlayFadeOutAnimation(GameObject transitionCanvas)
    {
        transitionCanvas.GetComponent<Animator>().SetTrigger("FadeOut");
    }

    public void PlayFadeInAnimation(GameObject transitionCanvas)
    {
        transitionCanvas.GetComponent<Animator>().SetTrigger("FadeIn");
    }
}
