﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransitionManager : Singleton<SceneTransitionManager>
{
    private AsyncOperation sceneAsync; // Operazione asincrona quando facciamo una transizione tra scena a scena
    private FadeTransitionManager fadeTransitionManager;
    private Scene currentScene;

    private void Start()
    {
        fadeTransitionManager = GetComponent<FadeTransitionManager>();
        GameObject fade = fadeTransitionManager.InstantiateNewFadeInCanvas();
        fadeTransitionManager.PlayFadeInAnimation(fade);
        currentScene = SceneManager.GetActiveScene();
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene() != currentScene)
        {
            currentScene = SceneManager.GetActiveScene();
            GameObject fade = fadeTransitionManager.InstantiateNewFadeInCanvas();
            fadeTransitionManager.PlayFadeInAnimation(fade);
        }
    }

    public void LoadSceneDelayed(string sceneName, float delay)
    {
        StartCoroutine(DelayedLoad(sceneName, delay));
    }

    private IEnumerator DelayedLoad(string sceneName, float timeToDelay)
    {
        yield return new WaitForSeconds(timeToDelay);
        GoToScene(sceneName, new List<GameObject>());
    }

    public void GoToScene(string sceneName, List<GameObject> objectsToMove)
    {
        StartCoroutine(PlayFadeOut(sceneName, objectsToMove));
    }

    private IEnumerator PlayFadeOut(string sceneName, List<GameObject> objectsToMove)
    {
        GameObject fade = fadeTransitionManager.InstantiateNewFadeOutCanvas();
        fadeTransitionManager.PlayFadeOutAnimation(fade);
        yield return new WaitForSeconds(4.0f);
        StartCoroutine(LoadScene(sceneName, objectsToMove));
    }

    private IEnumerator LoadScene(string sceneName, List<GameObject> objectsToMove)
    {
        SceneManager.LoadSceneAsync(sceneName); // Carica la scena in background

        SceneManager.sceneLoaded += (newScene, mode) =>
        {
            SceneManager.SetActiveScene(newScene);
        }; // Lambda, quando finisce di caricare la nuova scena in background allora esegue il comando

        Scene sceneToLoad = SceneManager.GetSceneByName(sceneName);
        foreach(GameObject obj in objectsToMove)
        {
            SceneManager.MoveGameObjectToScene(obj, sceneToLoad);
        }

        yield return null; // Con yield l'enumeratore restituisce un elemento alla volta
    }

    public void LoadeSceneWithName(string sceneName)
    {
        switch (sceneName)
        {
            case "1":
                Debug.Log("Load Scene 1");
                LoadSceneDelayed(PocketDroidsConstants.SCENE_3, 1.0f);
                break;
            case "2":
                Debug.Log("Load Scene 2");
                LoadSceneDelayed(PocketDroidsConstants.SCENE_3, 1.0f);
                break;
            case "3":
                Debug.Log("Load Scene 3");
                LoadSceneDelayed(PocketDroidsConstants.SCENE_3, 1.0f);
                break;
            case "4":
                Debug.Log("Load Scene 4");
                LoadSceneDelayed(PocketDroidsConstants.SCENE_3, 1.0f);
                break;
            case "5":
                Debug.Log("Load Scene 5");
                LoadSceneDelayed(PocketDroidsConstants.SCENE_3, 1.0f);
                break;
            case "6":
                Debug.Log("Load Scene 6");
                LoadSceneDelayed(PocketDroidsConstants.SCENE_3, 1.0f);
                break;
            case "7":
                Debug.Log("Load Scene 7");
                LoadSceneDelayed(PocketDroidsConstants.SCENE_3, 1.0f);
                break;
            case "8":
                Debug.Log("Load Scene 8");
                LoadSceneDelayed(PocketDroidsConstants.SCENE_3, 1.0f);
                break;
            case "9":
                Debug.Log("Load Scene 9");
                LoadSceneDelayed(PocketDroidsConstants.SCENE_3, 1.0f);
                break;
            case "10":
                Debug.Log("Load Scene 10");
                LoadSceneDelayed(PocketDroidsConstants.SCENE_3, 1.0f);
                break;
            case "11":
                LoadSceneDelayed(PocketDroidsConstants.SCENE_3, 1.0f);
                break;
            case "12":
                Debug.Log("Load Scene 12");
                LoadSceneDelayed(PocketDroidsConstants.SCENE_3, 1.0f);
                break;
            case "13":
                Debug.Log("Load Scene 13");
                LoadSceneDelayed(PocketDroidsConstants.SCENE_3, 1.0f);
                break;
            case "14":
                Debug.Log("Load Scene 14");
                LoadSceneDelayed(PocketDroidsConstants.SCENE_3, 1.0f);
                break;
            case "15":
                Debug.Log("Load Scene 15");
                LoadSceneDelayed(PocketDroidsConstants.SCENE_3, 1.0f);
                break;
            case "16":
                Debug.Log("Load Scene 16");
                LoadSceneDelayed(PocketDroidsConstants.SCENE_3, 1.0f);
                break;
            case "17":
                Debug.Log("Load Scene 17");
                LoadSceneDelayed(PocketDroidsConstants.SCENE_3, 1.0f);
                break;
            default:
                Debug.LogError("Cannot find scene with such a name");
                //LoadSceneDelayed(PocketDroidsConstants.SCENE_WORLD, 1.0f);
                break;
        }
    }
}
