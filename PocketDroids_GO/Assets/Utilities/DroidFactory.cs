﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

/** Class that allow us to spawn droids into the scene */
public class DroidFactory : Singleton<DroidFactory>
{
    [SerializeField] private GameObject[] availableDroids;
    [SerializeField] private float waitTime = 180.0f;
    [SerializeField] private int startingDroids = 5;
    [SerializeField] private float minRange = 5.0f;
    [SerializeField] private float maxRange = 50.0f;

    private List<GameObject> liveDroids = new List<GameObject>();
    private Droid selectedDroid;
    private Player player;

    public List<GameObject> LiveDroid { get { return liveDroids; } }

    public Droid SelectedDroid { get => selectedDroid; }
    public Player Player { get => player; }

    // We need to ensure that before this runs we have a valid list of droids, and a valid player
    private void Awake()
    {
        Assert.IsNotNull(availableDroids);
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameManager.Instance.CurrentPlayer;
        Assert.IsNotNull(player); // We don't type thin into the awake function cos at the start the game manager could be not instantiated
        // This allow us to instantiate initial droids on the map
        for (int i = 0; i < startingDroids; i++)
        {
            InstantiateDroid();
        }
        StartCoroutine(GenerateDroids());
    }

    public void DroidWasSelected(Droid droid)
    {
        this.selectedDroid = droid;
    }

    /** This function allow us to have a Coroutine going on with the normal script, who always spawn our Droids*/
    private IEnumerator GenerateDroids()
    {
        while (true)
        {
            InstantiateDroid();
            yield return new WaitForSeconds(waitTime);
        }
    }

    private void InstantiateDroid()
    {
        int index = Random.Range(0, availableDroids.Length); // So at the most we will get availableDroids.Length-1 droid spawned
        float x = Player.transform.position.x + GenerateRange();
        float z = Player.transform.position.z + GenerateRange();
        float y = Player.transform.position.y; // not adding the range in the y cos otherwise the droids could spawn in the air, or even inside the ground
        liveDroids.Add(Instantiate(availableDroids[index], new Vector3(x, y, z), Quaternion.identity));
    }

    private float GenerateRange()
    {
        float randomNum = Random.Range(minRange, maxRange); // For the float instead the Range is inclusive
        bool isPositive = Random.Range(0, 10) < 5;
        return randomNum * (isPositive ? 1 : -1);
    }
    
}
