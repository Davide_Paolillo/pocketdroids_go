﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreUpdater : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;

    private int score;

    public int Score { get => score; }

    private void Start()
    {
        score = 0;
        text.text = score.ToString();
    }

    public virtual void UpdateScore(int scoreToAdd)
    {
        score += scoreToAdd;
        text.text = score.ToString();
    }
}
