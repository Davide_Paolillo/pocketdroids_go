﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class CaptureSceneManager : PocketDroidsSceneManager
{
    [SerializeField] private int maxThrowAttempts = 3;
    [SerializeField] private GameObject orb;
    [SerializeField] private int xpToAdd = 10;

    private Vector3 spawnPoint;
    private int currentThrowAttempts;
    private CaptureSceneStatus status;

    public int CurrentThrowAttempts { get => currentThrowAttempts; }
    public int MaxThrowAttempts { get => maxThrowAttempts; }
    public CaptureSceneStatus Status { get => status; }

    private void Awake()
    {
        status = CaptureSceneStatus.InProgress;
        spawnPoint = orb.transform.position;
        Assert.IsNotNull(orb);
    }

    private void Start()
    {
        maxThrowAttempts = CalculateMaxThrows();
        currentThrowAttempts = maxThrowAttempts;
    }

    // Level design function, this allow us to determin how many orbs can the player obtain, and in wich way he can obtain them
    private int CalculateMaxThrows()
    {
        return maxThrowAttempts;
    }

    public void OrbDestroyed()
    {
        currentThrowAttempts--;
        if (currentThrowAttempts <= 0)
        {
            if(status != CaptureSceneStatus.Successful)
            {
                status = CaptureSceneStatus.Failed;
                Invoke("MoveToWorldScene", 2.0f); // Invoking the function 2 seconds after the collsion
            }
        } else
        {
            Instantiate(orb, spawnPoint, Quaternion.identity);
        }
    }

    public override void droidCollision(GameObject droid, Collision collision)
    {
        base.droidCollision(droid, collision);
        status = CaptureSceneStatus.Successful;
        Invoke("MoveToWorldScene", 2.0f); // Invoking the function seconds after the collsion
    }

    public void BinCentered()
    {
        status = CaptureSceneStatus.Successful;
        ExperienceManager.Instance.UpdateExperience(xpToAdd * currentThrowAttempts);
        Invoke("MoveToWorldScene", 2.0f); // Invoking the function seconds after the collsion
    }

    private void MoveToWorldScene()
    {
        SceneTransitionManager.Instance.GoToScene(PocketDroidsConstants.SCENE_WORLD, new List<GameObject>()); // Using a new list cos we don't have anything to pass right now
    }

    public override void droidTapped(GameObject droid)
    {
        print("CaptureSceneManager.droidTapped activated");
    }

    public override void playerTapped(GameObject player)
    {
        print("CaptureSceneManager.playerTapped activated");
    }
}
